# greenhouse-api

The API for greenhouse and all its features. Built using FastAPI and deployed to [Deta](https://www.deta.sh/).

Uses the Greenhouse Engine, was is used as a submodule within this repository.

## Tech Stack
* Python
* FastAPI
* FastAPI_Users
* MongoDB

## Running
To start, while in base directory run:
```
pipenv install
pipenv shell
uvicorn app.main:app --reload
```

The reload flag is for test environments, as it automatically checks for code changes and reloads.

If running locally it should have a local url and add `/docs` should lead to docmentation for testing end points.

## Deploying to Deta

In the base folder run below to make requirements file:
```
pipenv run pip freeze > app/requirements.txt
```

Then remove the reference to greenhouse-engine if exists in requirements file as it is a local link and will stop deployment.

Now CD into the app folder:
```
cd app
```

Next, push any environment files required, making sure ENVIRONMENT is set to production and database connection strings connect to production DB:

```
deta update -e .env 
```

Finally, deploy the new code:

```
deta deploy
```

## Environment Vars

Put in a .env file at the base folder and should be loaded automatically by FastAPI.

```
# Testing, production, etc
ENVIRONMENT=testing

# Where the engine weights are located
WEIGHTS_LOC=

# Location of the excel to extract weights from, not required in this
model_excel_loc=

# Key for linking to mattermost
MM_HOOK_KEY=

# MongoDB database connection string
MDB_CONN_STR=
```
