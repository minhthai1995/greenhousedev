from pydantic import BaseModel
from pydantic.fields import Field
from pydantic import validator
import re

street_suffix_dict = {
    "ALLEY": "ALLY",
    "ARCADE": "ARC",
    "AVENUE": "AVE",
    "BOULEVARD": "BVD",
    "BYPASS": "BYPA",
    "CIRCUIT": "CCT",
    "CLOSE": "CL",
    "CORNER": "CRN",
    "COURT": "CT",
    "CRESCENT": "CR",
    "CUL-DE-SAC": "CDS",
    "DRIVE": "DR",
    "ESPLANADE": "ESP",
    "GREEN": "GRN",
    "GROVE": "GR",
    "HIGHWAY": "HWY",
    "JUNCTION": "JNC",
    "PARADE": "PDE",
    "PLACE": "PL",
    "RIDGE": "RDGE",
    "ROAD": "RD",
    "SQUARE": "SQ",
    "STREET": "ST",
    "TERRACE": "TCE",
}

state_dict = {
    "SOUTH AUSTRALIA": "SA",
    "WESTERN AUSTRALIA": "WA",
    "TASMANIA": "TAS",
    "NEW SOUTH WALES": "NSW",
    "VICTORIA": "VIC",
    "NORTHERN TERRITORY": "NT",
    "QUEENSLAND": "QLD",
    "AUSTRALIAN CAPITAL TERRITORY": "ACT",
}


class QuestionAnswer(BaseModel):
    question_id: str
    answer: str


class Improvement(QuestionAnswer):
    score_modifier: float

class Recommendation(QuestionAnswer):
    item_name: str
    score_modifier: float
    environmental_improvement: float

class Address(BaseModel):
    street_number: str = Field(..., example="123A")
    street_name: str = Field(..., example="SOME ST")
    suburb: str = Field(..., example="SYDNEY")
    postcode: str = Field(..., min_length=4, max_length=4, example="0000")
    state: str = Field(..., example="NSW")

    # DB has values stored in all caps, so make sure values match
    @validator("street_number", "street_name", "suburb", "state")
    def to_upper(cls, v):
        return v.upper()

    @validator("street_name")
    def fix_street_suffx(cls, v):
        """
        Standardize known street suffix to short forms.
        """
        v = v.split()

        v_len = len(v)

        if v_len == 1:
            return

        street_suffix = v[v_len - 1]

        for full, short in street_suffix_dict.items():
            if street_suffix == full:
                v[v_len - 1] = short
                break
        v = " ".join(v)
        return v

    @validator("state")
    def fix_state_name(cls, v):
        """
        Accepts full state names but standardize to state code.
        """
        found = False
        to_abbr = None
        for full, short in state_dict.items():
            if v == full:
                to_abbr = short

            if to_abbr == short or v == short:
                found = True

        if not found:
            raise ValueError("Invalid state")

        if to_abbr:
            v = to_abbr

        return v

    @validator("postcode")
    def check_numbers(cls, v):
        """
        Make sure the postcode only contains numbers.
        """
        matched = re.match("^[0-9]*$", v)

        if not bool(matched):
            raise ValueError("Postcode can only contain numbers")

        return v

    class Config:
        allow_population_by_field_name = True

class BasixQuestions(BaseModel):
    certificate_date: int = Field(..., example=4547)
    financial_year: str = Field(..., example="2011/2012")
    regional_or_metropolitan: str = Field(..., example="metro")
    completion_receipt_date: int = Field(..., example=0)
    dwelling_type: str = Field(..., example="separate house")
    no_of_bedrooms: int = Field(..., example=3)
    site_area_m2: float = Field(..., example=304.3)
    total_floor_area_m2: float = Field(..., example=106.0)
    garden_and_lawn_area_m2: float = Field(..., example=124.0)
    low_water_species_area_m2: float = Field(..., example=-10.0)
    thermal_comfort_method: str = Field(..., example="Simulation")
    nathers_equivalent_star_rating: float = Field(..., example=6.5)
    showerhead_rating: str = Field(..., example="3 star (> 7.5 but <  9 L/min)")
    toilet_rating: str = Field(..., example="4 star")
    kitchen_taps_rating: str = Field(..., example="4 star")
    bathroom_taps_rating: str = Field(..., example="4 star")
    rw_tank_roof_collection_area_m2: float = Field(..., example=134.0)
    rw_tank_volume_l: float = Field(..., example=4500.0)
    storm_water_tank_volume_l: float = Field(..., example=-10)
    water_source_all_hot_water: str = Field(..., example="town water supply")
    water_source_toilets: str = Field(..., example="rainwater tank")
    water_source_household: str = Field(..., example="town water supply")
    water_source_garden_and_lawn: str = Field(..., example="rainwater tank")
    water_source_laundry: str = Field(..., example="rainwater tank")
    pool_volumne_kl: float = Field(..., example=-10.0)
    spa_volumne_kl: float = Field(..., example=-10.0)
    hot_water_system: str = Field(..., example="gas instantaneous")
    hot_water_system_rating: str = Field(..., example="5 star")
    heating_living_room_type: str = Field(..., example="1-phase airconditioning")
    heating_living_room_rating: str = Field(..., example="EER 3.0 - 3.5")
    heating_bedroom_type: str = Field(..., example="1-phase airconditioning")
    heating_bedroom_rating: str = Field(..., example="EER 3.0 - 3.5")
    cooling_living_room_type: str = Field(..., example="ceiling fans + 1-phase airconditioning")
    cooling_living_room_rating: str = Field(..., example="EER 2.5 - 3.0")
    cooling_bedroom_type: str = Field(..., example="ceiling fans + 1-phase airconditioning")
    cooling_bedroom_rating: str = Field(..., example="EER 2.5 - 3.0")
    lighting_no_of_living_room_energy_efficient: int = Field(..., example=0)
    lighting_no_of_bedroom_energy_efficient: int = Field(..., example=0)
    lighting_kitchen_energy_efficient: str = Field(..., example="False")
    pool_heating: str = Field(..., example="Not Available")
    spa_heating: str = Field(..., example="Not Available")
    oven_cooktop_type: str = Field(..., example="gas cooktop & electric oven")
    photovoltaic_output_kw: float = Field(..., example=1.5)