"""
FastAPI injectable dependencies.
"""


from fastapi.security import HTTPBasicCredentials, HTTPBasic
from fastapi import HTTPException, status, Depends
import secrets

security = HTTPBasic()


def check_auth(credentials: HTTPBasicCredentials = Depends(security)):
    """
    Forces injected method to require credentials and if wrong credentials
    raises error.
    Harcoded credentials as light security for API.
    """
    correct_username = secrets.compare_digest(credentials.username, "bubble5")
    correct_password = secrets.compare_digest(credentials.password, "geometric-pw1")
    if not (correct_username and correct_password):
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect credentials",
            headers={"WWW-Authenticate": "Basic"},
        )
