"""
Handles the MongoDB database. Get database and get user db connect to the same database
but use different methods as they link to different libraries with different requirements.
"""

from .settings import get_settings
import motor.motor_asyncio

client = motor.motor_asyncio.AsyncIOMotorClient(
    get_settings().mdb_conn_str, uuidRepresentation="standard"
)

db_name = (
    "greenhouseTestDB" if get_settings().environment == "testing" else "greenhouseDB"
)
db = client[db_name]



def get_database():
    return db
