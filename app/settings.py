"""
This settings file is for handling environment variables via pydantic.
"""

from pydantic import BaseSettings


class Settings(BaseSettings):
    environment: str
    mdb_conn_str: str
    mm_hook_key: str
    weights_loc: str
    basix_model_loc: str
    
    class Config:
        env_file = ".env"


settings = Settings()


def get_settings():
    """
    Gets the active global settings instance.
    """
    return settings
