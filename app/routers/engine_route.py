"""
Managing connections to the greenhouse engine.
"""


import pandas as pd

# Workaround for unable to relative import
import importlib
import sys

from fastapi import APIRouter, HTTPException, status, Query
from typing import List

from ..settings import Settings
from ..models import QuestionAnswer, Improvement, BasixQuestions, Recommendation

settings = Settings()


spec = importlib.util.spec_from_file_location(
    "engine", "./greenhouse-engine/greenhouse_engine/engine.py"
)
greenhouse_engine = importlib.util.module_from_spec(spec)
sys.modules[spec.name] = greenhouse_engine
spec.loader.exec_module(greenhouse_engine)
greenhouse_engine.set_weights(settings.weights_loc)
greenhouse_engine.load_basix_model(settings.basix_model_loc)
greenhouse_engine.set_mapper()
router = APIRouter(prefix="/engine", tags=["Engine"])



# the test API
@router.post("/test")
async def test_function():
    result = greenhouse_engine.test_dashboard_output()
    print(result)
    return result

@router.post("/")
async def calculate_score(
    questions: List[QuestionAnswer],
    avg_nathers: float = Query(..., gt=0, le=10),
    avg_condition_area: float = Query(...),
    avg_australian_score: float = Query(...),
    avg_home_size: float = Query(...),  
    climate_state: str = Query(...),
    avg_energy_usage: float = Query(...)
) -> dict:
    """
    Takes in a list of answered questions, average nathers and condition area and returns a score
    from the greenhouse engine package. Requires house_area and either year_built or last_renovation
    """
    questions_dict = {}

    for question in questions:
        questions_dict[question.question_id] = question.answer

    if (not "house_area" in questions_dict.keys()) or (
        not "year_built" in questions_dict.keys()
        and not "last_renovation" in questions_dict.keys()
    ):
        raise HTTPException(
            status.HTTP_400_BAD_REQUEST,
            "Requires questions house_area and either year_built or last_renovation",
        )

    score = greenhouse_engine.get_estimated_nathers_score(
        questions_dict, avg_nathers, avg_condition_area, avg_australian_score, avg_home_size, climate_state, avg_energy_usage
    )

    return score


@router.post("/improvements", response_model=List[Improvement])
async def find_improvements(questions: List[QuestionAnswer]) -> List[Improvement]:
    """
    Takes in a list of answered quetions and returns a list of answers that would score higher.
    """
    questions_list = []
    answers_list = []
    for question in questions:
        questions_list.append(question.question_id)
        answers_list.append(question.answer)

    improvements = greenhouse_engine.get_improvements(
        pd.DataFrame({"question_id": questions_list, "answer": answers_list})
    ).to_dict("records")

    return improvements


@router.post("/topimprovements", response_model=List[Improvement])
async def find_top_improvements(questions: List[QuestionAnswer]) -> List[Improvement]:
    """
    Takes in a list of answered quetions and for each question return the answer that would be the
    highest possible improvement.
    """
    questions_list = []
    answers_list = []
    for question in questions:
        questions_list.append(question.question_id)
        answers_list.append(question.answer)

    improvements = greenhouse_engine.get_top_improvements(
        pd.DataFrame({"question_id": questions_list, "answer": answers_list})
    ).to_dict("records")

    return improvements

@router.post("/recommendation")
async def find_recommendation(questions: List[QuestionAnswer]) -> List[Recommendation]:
    """
    Takes in a list of answered quetions and returns a list of answers that would score higher.
    """
    questions_list = []
    answers_list = []
    for question in questions:
        questions_list.append(question.question_id)
        answers_list.append(question.answer)

    recommendation = greenhouse_engine.get_recommendation(
        pd.DataFrame({"question_id": questions_list, "answer": answers_list})
    )

    return recommendation

@router.post("/getbasixenergyscore")
async def get_basix_energy_score(questions: BasixQuestions) -> float:
    """
    Takes in answers to required basix questions and returns a BASIX energy score.
    """
    return greenhouse_engine.get_basix_energy_score([list(questions.dict().values())])[0]

@router.post("/getresiliencescore")
async def get_resilience_score(housed_car : str, clear_gutters : str, downpipes_away : str, secured_fence: str) -> str:
    "Returns resilience score, all string values must be Yes or No. Returns red, amber or green."

    ans = ["yes", "no"]

    if (not housed_car.lower() in ans) or (not clear_gutters.lower() in ans) or (not downpipes_away.lower() in ans) or (not secured_fence.lower() in ans):
        raise HTTPException(
            status.HTTP_400_BAD_REQUEST,
            "All answers must be Yes or No",
            )

    housed_car = housed_car.lower() == "yes"
    clear_gutters = clear_gutters.lower() == "yes"
    downpipes_away = downpipes_away.lower() == "yes"
    secured_fence = secured_fence.lower() == "yes"

    return greenhouse_engine.get_resilience_score(housed_car, clear_gutters, downpipes_away, secured_fence)