"""
For handling and transforming any requests for data.
"""

from fastapi import APIRouter, Depends, status, HTTPException
from motor.motor_asyncio import AsyncIOMotorDatabase
from typing import List

from ..db import get_database
from ..models import Address, QuestionAnswer

router = APIRouter(tags=["Data"])


@router.get("/data/house/prefill-answers", response_model=List[QuestionAnswer])
async def get_house_prefill_answers(
    address: Address = Depends(Address),
    db: AsyncIOMotorDatabase = Depends(get_database),
) -> List[QuestionAnswer]:
    """
    Get prefill data of house from database and format it to return to UI.
    """

    print(address)

    house = await db["house.prefills"].find_one(
        {
            "Street Number": address.street_number,
            "Street Name": address.street_name,
            "Suburb": address.suburb,
            "Postcode": int(address.postcode),
            "State": address.state,
        }
    )

    if not house:
        return []  # TODO Return with status code 204 (no content)

    prefill_answers = []
    for key, value in house.items():
        if key != "_id":
            prefill_answers.append(QuestionAnswer(question_id=key, answer=value))

    return prefill_answers
