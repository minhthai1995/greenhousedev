import pytest
import httpx
import asyncio

from asgi_lifespan import LifespanManager
from motor.motor_asyncio import AsyncIOMotorClient
from fastapi import status

from app.main import app
from app.db import get_database

test_house_prefills = [
    {
        "Street Number": "23",
        "Street Name": "SATSUMA CR",
        "Suburb": "GOLDEN GROVE",
        "Postcode": {"$numberInt": "5125"},
        "State": "SA",
        "house_area": {"$numberDouble": "278.0"},
        "land_area": {"$numberDouble": "1010.0"},
        "year_built": {"$numberInt": "1999"},
        "sewerage_available": False,
        "water_available": False,
        "number_of_main_rooms": {"$numberInt": "10"},
        "wall_external_material": "Other",
        "car_accommodation": "Garage",
        "valai_value": {"$numberDouble": "578240.0"},
    },
    {
        "Street Number": "18",
        "Street Name": "RENWICK ST",
        "Suburb": "WEST BEACH",
        "Postcode": {"$numberInt": "5024"},
        "State": "SA",
        "house_area": {"$numberDouble": "275.0"},
        "land_area": {"$numberDouble": "773.0"},
        "year_built": {"$numberInt": "1960"},
        "sewerage_available": True,
        "water_available": True,
        "number_of_main_rooms": {"$numberInt": "9"},
        "number_of_stories": {"$numberInt": "1"},
        "wall_external_material": "Brick veneer",
        "car_accommodation": "Carport",
        "valai_value": {"$numberDouble": "1011920.0"},
    },
    {
        "Street Number": "1",
        "Street Name": "DAVENPORT TCE",
        "Suburb": "SEAVIEW DOWNS",
        "Postcode": {"$numberInt": "5049"},
        "State": "SA",
        "house_area": {"$numberDouble": "102.0"},
        "land_area": {"$numberDouble": "900.0"},
        "year_built": {"$numberInt": "1950"},
        "sewerage_available": False,
        "water_available": True,
        "number_of_main_rooms": {"$numberInt": "5"},
        "wall_external_material": "Other",
        "car_accommodation": "Double Garage",
        "valai_value": {"$numberDouble": "405880.0"},
    },
]


motor_client = AsyncIOMotorClient("mongodb://localhost:27017")
database_test = motor_client["greenhouseTestDB"]


def get_test_database():
    return database_test


@pytest.fixture(scope="session")
def event_loop():
    loop = asyncio.get_event_loop()
    yield loop
    loop.close()


@pytest.fixture
async def test_client():
    app.dependency_overrides[get_database] = get_test_database
    async with LifespanManager(app):
        async with httpx.AsyncClient(
            app=app, base_url="http://0.0.0.0:8000"
        ) as test_client:
            yield test_client


@pytest.fixture(autouse=True, scope="module")
async def initial_house_prefills():
    await database_test["house.prefills"].insert_many(test_house_prefills)

    yield test_house_prefills

    await motor_client.drop_database("greenhouseTestDB")


@pytest.mark.asyncio
class TestGetPrefillAnswers:
    async def test_existing(self, test_client=httpx.AsyncClient):
        house = test_house_prefills[1]

        params = {
            "street_number": house["Street Number"],
            "street_name": house["Street Name"],
            "suburb": house["Suburb"],
            "postcode": house["Postcode"],
            "state": house["State"],
        }

        response = await test_client.request(
            f"/data/house/prefill-answers", params=params
        )

        assert response.status_code == status.HTTP_200_OK

        json = response.json()
        assert json == str(house)

    # async def test_not_existing(self, test_client = httpx.AsyncClient):
