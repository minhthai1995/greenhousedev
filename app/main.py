from fastapi import FastAPI, Depends
from fastapi.middleware.cors import CORSMiddleware
from .routers import engine_route, data
from .dependencies import check_auth

app = FastAPI()
@app.get("/")
async def root():
    return {"message": "Hello World", "content": "succesful"}
    
app.include_router(engine_route.router, dependencies=[Depends(check_auth)])
app.include_router(data.router, dependencies=[Depends(check_auth)])